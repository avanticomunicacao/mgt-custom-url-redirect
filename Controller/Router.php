<?php
namespace Avanti\CustomUrlRedirect\Controller;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\Action\Redirect;
use Magento\UrlRewrite\Model\UrlRewrite;
use Avanti\CustomUrlRedirect\Logger\Logger as LoggerUrlRedirect;



class Router implements RouterInterface
{
    private $response;
    private $actionFactory;
    private $urlRewrite;
    private $logger;

    const STRING_SEARCH = '/resultadopesquisa';

    public function __construct(
        ResponseInterface $response,
        ActionFactory $actionFactory,
        UrlRewrite $urlRewrite,
        LoggerUrlRedirect $logger
    ) {
        $this->response = $response;
        $this->actionFactory = $actionFactory;
        $this->urlRewrite = $urlRewrite;
        $this->logger = $logger;
    }

    /**
     * Validate and Match
     *
     * @param RequestInterface $request
     */
    public function match(RequestInterface $request)
    {
        $this->logger->info("Request Url: \n");
        $this->logger->info($request->getRequestString());

        $rewrite = $this->checkRewrite($request->getRequestString());

        $this->logger->info("resultado do checkRewrite");
        $this->logger->info(print_r($rewrite));

        if (is_array($rewrite) && $rewrite['redirect_type'] != 0) {
            return $this->redirect($request, $rewrite['target_path'], $rewrite['redirect_type']);
        }

        if ($request->getRequestString() === self::STRING_SEARCH) {
            return $this->redirect($request, "/", "302");
        }

        return null;
    }

    /**
     * Redirect to target URL
     *
     * @param RequestInterface $request
     * @param string $url
     * @param int $code
     * @return ActionInterface
     */
    protected function redirect($request, $url, $code)
    {
        $this->response->setRedirect($url, $code);
        $request->setDispatched(true);

        return $this->actionFactory->create(Redirect::class);
    }

    /**
     * Método para Checar se existe algum Rewrite para essa url
     * @param $requestUrl
     * @return boolean|array
     */
    protected function checkRewrite($requestUrl)
    {
        $this->logger->info("Request url que chegou no checkRewrite: \n");
        $this->logger->info($requestUrl);

        if (empty($requestUrl)) {
            return  false;
        }

        $requestUrl = ltrim($requestUrl, $requestUrl[0]);

        $this->logger->info("requet url tirando a primeira '/' \n");
        $this->logger->info($requestUrl);

        $requestUrl = $this->changePlusSign($requestUrl);
        $this->logger->info("Request URL depois de trocar o sinal de + \n");
        $this->logger->info($requestUrl);

        $requestUrl = urldecode($requestUrl);
        $this->logger->info("Request Url depois de passar pelo urldecode \n");
        $this->logger->info($requestUrl);

        $urlRewriteCollection = $this->urlRewrite->getCollection()->addFieldToFilter('request_path', $requestUrl);
        $rewrite = $urlRewriteCollection->getFirstItem();

        $this->logger->info("depois de buscar e dar um getFirstItem:\n");
        if ($urlRewriteCollection->getFirstItem()->getId()) {
            $this->logger->info("entrou dentro de getFirstItem -> getId()");
            return array(
                'redirect_type' => $rewrite->getData('redirect_type'),
                'target_path' => $rewrite->getData('target_path')
            );
        }
        return false;
    }

    protected  function changePlusSign($string)
    {
        return str_replace("+", "%2B", $string);
    }
}
