# Módulo de Redirecionamento #

## Uso ##
Esse módulo faz com o que o magento comece a reconhecer rotas com query strings na 
parte de URL Rewrite Default do Magento. Para acessar vá para: **Admin -> Marketing ->
SEO & Search -> Url Rewrite**

Exemplo:

![Image of Url Rewrite](docs/rota_exemplo.png)

No exemplo da imagem acima, ao digitar a url da coluna "Request Path",
o magento informa que a página não existe(erro 404), ao invés de redirecionar o 
usuário para a rota localizada na coluna "Target Path". 
Com esse módulo o redirecionamento ocorre sem problemas.
