<?php
namespace Avanti\CustomUrlRedirect\Logger;

use Magento\Framework\Logger\Handler\Base as HandlerBase;
use Monolog\Logger;

class Handler extends HandlerBase
{
    protected $loggerType = Logger::INFO;
    protected $fileName = '/var/log/customurlredirect.log';
}
